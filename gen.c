#include <stdlib.h>
#include <string.h>

#include "sudoku.h"
#include "aleatoire.h"


bool GenerationBacktracking (int grille[N_2][N_2], int pos, unsigned aleatoire[][N_2])
{
    if (pos < TOTAL_GRID_SZ)
    {
        const unsigned i = pos/N_2, j = pos%N_2;
        const unsigned b = N*(i/N)+(j/N);
        for (unsigned n=0; n < N_2; n++)
        {
            int k = aleatoire[pos][n];
            if ( !ligne[i][k] && !colonne[j][k] && !bloc[b][k] )
            {
                ligne[i][k] = colonne[j][k] = bloc[b][k] = true;
                // Appel recursif
                if ( GenerationBacktracking (grille, pos+1, aleatoire) )
                {
                    grille[i][j] = k+base;
                    return true;
                }
                ligne[i][k] = colonne[j][k] = bloc[b][k] = false;
            }
        }
        return false;
    }
    return true;
}

#include "dlx.h"

static Node* add_row (Node* header[], unsigned l, unsigned c, int val)
{
    unsigned pos[4];
    unsigned k = val-base;
    // calcule les positions pour les contraintes
    pos[0] = (l*N_2)+c;
    pos[1] = (l*N_2)+k;
    pos[2] = (c*N_2)+k;
    pos[3] = ((N*(l/N)+c/N)*N_2)+k;
    // ajoute les contraintes au dancing links
    Node* last = NULL;
    for (unsigned i=0; i < 4; i++)
        last = CreateNode (header[i*N_2*N_2+pos[i]], last);
    return last;
}

static void delete_row (Node* row)
{
    //printf("deleting row... ");
    row->left->right = NULL;  // casse le cercle
    for (Node *iter = row, *next = NULL; iter != NULL; iter = next)
    {
        next = iter->right;
        iter->up->down = iter->down;
        iter->down->up = iter->up;
        iter->head->size--;
        free(iter);
    }
    //printf("ok\n");
}

static unsigned TraitementGrille (int grille[N_2][N_2])
{
    unsigned cases_restantes = TOTAL_GRID_SZ;
    Node* header[4*N_2*N_2];
    Node* rows[N_2-1];

    // cree la racine
    Node* root = CreateRoot();
    // cree les headers
    for (unsigned i=0; i < 4*N_2*N_2; i++)
        header[i] = CreateHeader(root);

    for (unsigned l=0; l < N_2; l++)
        for (unsigned c=0; c < N_2; c++)
            add_row (header, l, c, grille[l][c]);


    //int cmpt = 0;

    InitGenerateur (0, TOTAL_GRID_SZ-1);
    for (unsigned n=0; n < TOTAL_GRID_SZ; n++)
    {
        unsigned pos = ReturnAleatoire();
        unsigned i = pos/N_2, j = pos%N_2;
        if (grille[i][j])
        {
            unsigned p = 0;
            for (int k=base; k < base+N_2; k++)
                if (k != grille[i][j])
                    rows[p++] = add_row (header, i, j, k);

            int old = grille[i][j];
            grille[i][j] = 0;

            // teste si la case peut etre enlevée
            if ( uniq (root) )
                cases_restantes--;
            else
            {
                grille[i][j] = old;
                for (int k=0; k < p; k++)
                    delete_row ( rows[k] );
            }
            //printf("%d)\n", cmpt++),system("pause");
        }
    }
    TermineGenerateur();
    DeleteAll (root);

    return cases_restantes;
}
//static void cloak_row (Node* row)
//{
////printf("cloak...");
//    for (Node *iter = row->right; iter != row; iter = iter->right)
//    {
//        iter->up->down = iter->down;
//        iter->down->up = iter->up;
//        iter->head->size--;
//    }
//    row->up->down = row->down;
//    row->down->up = row->up;
//    row->head->size--;
////printf(" ok\n");
//}
//
//static void uncloak_row (Node* row)
//{
////printf("uncloak...");
//    for (Node *iter = row->right; iter != row; iter = iter->right)
//    {
//        iter->up->down = iter;
//        iter->down->up = iter;
//        iter->head->size++;
//    }
//    row->up->down = row;
//    row->down->up = row;
//    row->head->size++;
////printf(" ok\n");
//}
//
//static unsigned TraitementGrille (int grille[N_2][N_2])
//{
//    unsigned cases_restantes = TOTAL_GRID_SZ;
//    Node* header[4*N_2*N_2];
//    Node* rows[N_2*N_2*N_2];
//
//    // cree la racine
//    Node* root = CreateRoot();
//    // cree les headers
//    for (unsigned i=0; i < 4*N_2*N_2; i++)
//        header[i] = CreateHeader(root);
//
//    for (unsigned l=0; l < N_2; l++)
//        for (unsigned c=0; c < N_2; c++)
//            for (int k=0; k < N_2; k++)
//            {
//                rows[(l*N_2*N_2)+(c*N_2)+k] = add_row (header, l, c, k+base);
//                if (k+base != grille[l][c])
//                    cloak_row ( rows[(l*N_2*N_2)+(c*N_2)+k] );
//            }
//
//    int cmpt = 0;
//
//    InitGenerateur (0, TOTAL_GRID_SZ-1);
//    for (unsigned n=0; n < TOTAL_GRID_SZ; n++)
//    {
//        unsigned pos = ReturnAleatoire();
//        unsigned i = pos/N_2, j = pos%N_2;
//        if (grille[i][j])
//        {
//            for (int k=0; k < N_2; k++)
//                if (k+base != grille[i][j])
//                    uncloak_row ( rows[(i*N_2*N_2)+(j*N_2)+k] );
//
//            int old = grille[i][j];
//            grille[i][j] = 0;
//
//            // teste si la case peut etre enlevée
//            if ( uniq (root) )
//                puts("Uniq !"),cases_restantes--;
//            else
//            {
//                grille[i][j] = old;
//                for (int k=0; k < N_2; k++)
//                    if (k+base != grille[i][j])
//                        cloak_row ( rows[(i*N_2*N_2)+(j*N_2)+k] );
//            }
//            printf("%d)\n", cmpt++),system("pause");
//        }
//    }
//    TermineGenerateur();
//    DeleteAll (root);
//
//    return cases_restantes;
//}

unsigned GenerationAleatoire (int grille[N_2][N_2])
{
    unsigned n, i;
    unsigned val[TOTAL_GRID_SZ][N_2] = { {0}, {0} };

    InitGenerateur (0, N_2-1);
    memset(grille, 0, N_2*sizeof *grille);

    for (i=0; i<TOTAL_GRID_SZ; i++)
        for (n=0; n < N_2; n++)
            val[i][n] = ReturnAleatoire();

    memset(ligne, 0, N_2*sizeof *ligne);
    memset(colonne, 0, N_2*sizeof *colonne);
    memset(bloc, 0, N_2*sizeof *bloc);

    GenerationBacktracking (grille, 0, val);
    TermineGenerateur();

    //puts("Generation terminee !");

    return TraitementGrille (grille);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>



#include "sudoku.h"
#include "aleatoire.h"
#include "circular_list.h"




static int UniqBacktrackingFn (CIRCL_LIST* root)
{
    if (root == root->next)
        return 1;

    // cherche la case avec un nombre de valeurs possibles minimal
    CIRCL_LIST *pos = root->next;
    for (CIRCL_LIST *iter = pos->next; iter != root; iter = iter->next)
    {
        if (iter->nbValeursPossibles < pos->nbValeursPossibles)
            pos = iter;
    }

    const unsigned min = pos->nbValeursPossibles;
    if (min == 0) return 0;

    const unsigned i = pos->i, j = pos->j, b = pos->b;

    // supprimme le noeud de la liste
    pos->prev->next = pos->next;
    pos->next->prev = pos->prev;

    // (optimisation) enregistre les cases voisines
    CIRCL_LIST* voisins[2*(N_2-1)+(N-1)*(N-1)];
    unsigned n = 0;
    for (CIRCL_LIST *iter = root->next; iter != root; iter = iter->next)
        if ( iter->i == i || iter->j == j || iter->b == b )
            voisins[n++] = iter;

    int ret = 0;
    unsigned nb = 0;
    for (unsigned k=0; k < N_2 && ret <= 1; k++)
    {
        // si on peut placer k
        if ( !ligne[i][k] && !colonne[j][k] && !bloc[b][k] )
        {
            // on actualise le champ 'nbValeursPossibles' dans la liste
            for (CIRCL_LIST** it = voisins; it != voisins+n; it++)
                if ( !ligne[(*it)->i][k] && !colonne[(*it)->j][k] && !bloc[(*it)->b][k] )
                    (*it)->nbValeursPossibles--;

            // Ajoute k aux valeurs enregistrées
            ligne[i][k] = colonne[j][k] = bloc[b][k] = true;

            ret += UniqBacktrackingFn (root);

            // Supprime k des valeurs enregistrées
            ligne[i][k] = colonne[j][k] = bloc[b][k] = false;

            // on re-actualise le champ 'nbValeursPossibles' dans la liste
            for (CIRCL_LIST** it = voisins; it != voisins+n; it++)
                if ( !ligne[(*it)->i][k] && !colonne[(*it)->j][k] && !bloc[(*it)->b][k] )
                    (*it)->nbValeursPossibles++;

            // (optimisation) si on a déja traité toiutes les valeurs possibles, on sort
            if (++nb == min) break;
        }
    }

    // on restaure le noeud initial
    pos->prev->next = pos;
    pos->next->prev = pos;

    return ret;
}

static bool Uniq (CIRCL_LIST* positions)
{
    return UniqBacktrackingFn (positions) == 1;
}

static unsigned nb_possibles (int i, int j)
{
    unsigned ret = 0;
    for (int k=0; k < 9; k++)
        if ( !ligne[i][k] && !colonne[j][k] && !bloc[N*(i/N)+(j/N)][k] )
            ret++;
    return ret;
}

static unsigned TraitementGrille (int grille[N_2][N_2])
{
    unsigned cases_restantes = TOTAL_GRID_SZ;
    int k;
/*
    for (unsigned i=0; i < N_2; i++)
        for (unsigned j=0; j < N_2; j++)
            ligne[i][j] = colonne[i][j] = bloc[i][j] = false;
*/
    memset(ligne, 0, N_2*sizeof *ligne);
    memset(colonne, 0, N_2*sizeof *colonne);
    memset(bloc, 0, N_2*sizeof *bloc);

    for (unsigned i=0; i < N_2; i++)
        for (unsigned j=0; j < N_2; j++)
            if ( (k = grille[i][j]) != 0 )
                ligne[i][k-base] = colonne[j][k-base] = bloc[N*(i/N)+(j/N)][k-base] = true;

    CIRCL_LIST* root = createRoot();
    InitGenerateur (0, TOTAL_GRID_SZ-1);

    for (unsigned n=0; n < TOTAL_GRID_SZ; n++)
    {
        // tire une case au hasard
        unsigned pos = ReturnAleatoire();
        unsigned i = pos/N_2, j = pos%N_2;
        // si la case est pleine (normalement elle l'est)
        if ( grille[i][j] )
        {
            int val = grille[i][j];

            // crée un nouveau noeud et l'initialise
            CIRCL_LIST* node = circular_list_cons ( root );
            node->i = i, node->j = j, node->b = N*(i/N)+(j/N);

            // marque val comme inutilisée dans le tableau des valeurs possibles
            ligne[i][val-base] = colonne[j][val-base] = bloc[N*(i/N)+(j/N)][val-base] = false;

            // actualise le champ nbPossibles dans les elements de la liste
            for (CIRCL_LIST* iter = root->next; iter != root; iter = iter->next)
                iter->nbValeursPossibles = nb_possibles(iter->i,iter->j);

            // teste si la case peut etre enlevée
            if ( Uniq ( root ) )
            {
                grille[i][j] = 0;
                cases_restantes--;
            }
            else
            {
                ligne[i][val-base] = colonne[j][val-base] = bloc[N*(i/N)+(j/N)][val-base] = true;
                circular_list_delete (node);
            }
        }
    }

    TermineGenerateur();
    deleteAll (root);

    return cases_restantes;
}

unsigned GenerationAleatoire2 (int grille[N_2][N_2])
{
    // initialise une grille
    memset(grille, 0, N_2*sizeof *grille);

    unsigned val[TOTAL_GRID_SZ][N_2] = { {0}, {0} };
    // initialise le generateur aleatoire sans doublons
    InitGenerateur (0, N_2-1 );
    // prepare des listes (tableaux) de valeurs possibles mélangées
    for (unsigned i=0; i<TOTAL_GRID_SZ; i++)
        for (unsigned n=0; n < N_2; n++)
            val[i][n] = ReturnAleatoire();
    TermineGenerateur();

    // initialise les tableaux statiques des valeurs possibles
    memset(ligne, 0, N_2*sizeof *ligne);
    memset(colonne, 0, N_2*sizeof *colonne);
    memset(bloc, 0, N_2*sizeof *bloc);

    // genere une grille totalement remplie
    GenerationBacktracking (grille, 0, val);

    // elimine un maximum de cases, et retourne le nombre de cases restantes
    return TraitementGrille (grille);
}

#include "circular_list.h"



CIRCL_LIST* createRoot (void)
{
    CIRCL_LIST* node = (CIRCL_LIST*) malloc(sizeof *node);
    node->prev = node->next = node;
    return node;
}

CIRCL_LIST* circular_list_cons (CIRCL_LIST* root)
{
    CIRCL_LIST* node = (CIRCL_LIST*) malloc(sizeof *node);
    node->prev = root->prev;
    node->next = root;
    root->prev->next = node;
    root->prev = node;
    return node;
}

void circular_list_delete (CIRCL_LIST* node)
{
    node->prev->next = node->next;
    node->next->prev = node->prev;
    free(node);
}

void deleteAll (CIRCL_LIST* root)
{
    CIRCL_LIST *next, *iter;
    for (iter = root->next; iter != root; iter = next)
    {
        next = iter->next;
        free (iter);
    }
    free (root);
}

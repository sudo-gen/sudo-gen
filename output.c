#include <stdio.h>

#include "sudoku.h"


void OutputSudoku (int grille[N_2][N_2], FILE* fp)
{
    unsigned i, j;
    for (i=0; i<N_2; i++)
    {
        fputc('{', fp);
        for (j=0; j<N_2; j++)
        {
#if N_2 < 10
            fprintf(fp, "%u", grille[i][j] );
#else
            fprintf(fp, "%2u", grille[i][j] );
#endif

            if ( (j+1)%N_2 ) fputc( ',' ,fp);
        }
        fputs("},\n", fp);
    }
    fputc('\n', fp);
}


#include <cairo/cairo.h>


#define PNG_W       (500.)
#define PNG_H       (500.)
#define MARGIN      (80.)
void OutputSudokuToPng (int grid[N_2][N_2], const char* fontname, const char* output_filename)
{
    double width = PNG_W - MARGIN, height = PNG_H - MARGIN;
    double grid_x = MARGIN / 2, grid_y = MARGIN / 2;
    double border_w = width / 120, border_h = height / 120;
    double box_w = (width - (N+1)*border_w*2 - (N*(N-1))*border_w)/N_2, box_h = (height - (N+1)*border_h*2 - (N*(N-1))*border_h)/N_2;
    unsigned i,j;
    double x,y;
    char num[3] = {0};

    cairo_surface_t *surface;
    cairo_t *cr;
    cairo_text_extents_t te;

    surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, PNG_W, PNG_H);
    cr = cairo_create (surface);

    cairo_set_source_rgb (cr, 1., 1., 1.); /* BLANC */
    cairo_paint (cr);

    cairo_set_source_rgb (cr, 0., 0., 0.); /* NOIR */
    cairo_rectangle (cr, grid_x, grid_y, width, height);
    cairo_fill (cr);


    for (i=0, x=grid_x; i < N_2; i++, x+=box_w)
    {
        x += (i%N == 0 ? 2*border_w : border_w);
        for (j=0, y=grid_y; j < N_2; j++, y+=box_h)
        {
            y += (j%N == 0 ? 2*border_h : border_h);
            cairo_set_source_rgb (cr, 1., 1., 1.); /* BLANC */
            cairo_rectangle (cr, x, y, box_w, box_h);
            cairo_fill (cr);

            if (grid[i][j] != 0)
            {
                sprintf(num, "%d", grid[i][j]);
                cairo_set_source_rgb (cr, 0., 0., 0.); /* NOIR */
                cairo_select_font_face (cr, fontname, CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
                cairo_set_font_size (cr, box_h * 0.85);
                cairo_text_extents (cr, num, &te);
                cairo_move_to (cr, x + box_w / 2 - te.width / 2 - te.x_bearing, y + box_h / 2 - te.height / 2 - te.y_bearing);
                cairo_show_text (cr, num);
            }
        }
    }

    cairo_surface_write_to_png (surface, output_filename);

    cairo_destroy (cr);
    cairo_surface_destroy (surface);
}


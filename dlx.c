#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>


#include "dlx.h"


Node* CreateRoot (void)
{
    Node* root = (Node*) malloc (sizeof *root);
    root->right = root->left = root->up = root->down = root;
    root->head = NULL;
    return root;
}

Node* CreateHeader (Node* root)
{
    Node* header = (Node*) malloc (sizeof *header);
    // gestion des liens right & left
    header->left = root->left;
    header->right = root;
    root->left->right = header;
    root->left = header;
    // gestion des liens up & down
    header->up = header->down = header;
    // initialise nbElements
    header->size = 0;
    header->head = NULL;
    return header;
}

Node* CreateNode (Node* header, Node* last)
{
    Node* node = (Node*) malloc (sizeof *node);
    // gestion des liens up & down
    node->up = header->up;
    node->down = header;
    header->up->down = node;
    header->up = node;
    node->head = header;
    // gestion des liens right & left
    if (last == NULL)
    {
        node->right = node->left = node;
    }
    else
    {
        node->left = last;
        node->right = last->right;
        last->right->left = node;
        last->right = node;
    }
    // incremente nbElements du header
    header->size++;
    return node;
}

static void cover (Node* header)
{
    // supprime le header
    header->left->right = header->right;
    header->right->left = header->left;
    // traitement des noeuds
    for (Node* iter = header->down; iter != header; iter = iter->down)
    {
        for (Node* iter2 = iter->right; iter2 != iter; iter2 = iter2->right)
        {
            iter2->up->down = iter2->down;
            iter2->down->up = iter2->up;
            iter2->head->size--;
        }
    }
}

static void uncover (Node* header)
{
    // traitement des noeuds
    for (Node* iter = header->up; iter != header; iter = iter->up)
    {
        for (Node* iter2 = iter->left; iter2 != iter; iter2 = iter2->left)
        {
            iter2->up->down = iter2;
            iter2->down->up = iter2;
            iter2->head->size++;
        }
    }
    // restaure le header
    header->left->right = header;
    header->right->left = header;
}

// nb_solutions
static int search (Node* root)
{
    if (root == root->right)
        return 1;

    int ret = 0;

    // recherche la colonne avec le moins de noeuds
    Node* header = root->right;
    for (Node* iter = header->right; iter != root; iter = iter->right)
    {
        if (iter->size < header->size)
            header = iter;
    }

    // procedure
    cover(header);
    for (Node* iter = header->down; iter != header && ret <= 1; iter = iter->down)
    {
        //Node* row = iter;
        for (Node* iter2 = iter->right; iter2 != iter; iter2 = iter2->right)
            cover(iter2->head);

        ret += search (root);

        //iter = row, header = row->head;
        for (Node* iter2 = iter->left; iter2 != iter; iter2 = iter2->left)
            uncover(iter2->head);

        if (ret >= 2) break;
    }
    uncover(header);

    return ret;
}

void DeleteAll (Node* root)
{
    Node *nextr, *nextd;
    for (Node* iter = root->right; iter != root; iter = nextr)
    {
        for (Node* iter2 = iter->down; iter2 != iter; iter2 = nextd)
        {
            nextd = iter2->down;
            free (iter2);
        }
        nextr = iter->right;
        free (iter);
    }
    free (root);
}

bool uniq (Node* root)
{
    return search(root) == 1;
}

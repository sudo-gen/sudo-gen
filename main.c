#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "sudoku.h"



int main (void)
{
    time_t tm = time(NULL);
    srand(tm);
    unsigned n = 0;
    //unsigned found = 0;
    double tempsTotal = 0;
    int grille[N_2][N_2];
    char out[250];
    //FILE *fp = fopen("grilles_9x9_min_20.txt", "a");
    while (tempsTotal < 5)
    //while (n < 1000)
    //for (unsigned k=0; k<20; k++)
    {
        //printf("%ue grille...\n", k);
        clock_t t1 = clock();
        int nb = GenerationAleatoire2 (grille);
        tempsTotal += (double)(clock()-t1)/CLOCKS_PER_SEC;
        if (nb <= 20)
        {
//            fprintf(fp, "\nNombre cases : %d\n", nb);
            sprintf(out, "%d-%.3u.png", tm, n);
            OutputSudokuToPng (grille, "Sans", out);
            //OutputSudoku(grille, fp);
            //found++;
        }
        n++;
    }
    //fclose(fp);
    printf("Temps total : %gs\n%u grilles generees\n", tempsTotal, n);
    //printf("%u grille(s) trouvee(s)\n", found)
    return 0;
}

#ifndef _DLX_H
#define _DLX_H


typedef struct _node
{
    struct _node* left;
    struct _node* right;
    struct _node* up;
    struct _node* down;
    struct _node* head;
    unsigned size;
} Node;


Node* CreateRoot (void);
Node* CreateHeader (Node* root);
Node* CreateNode (Node* header, Node* last);
void DeleteAll (Node* root);
bool uniq (Node* root);


#endif

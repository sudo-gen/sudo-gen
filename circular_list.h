#ifndef CIRCULAR_LIST_H
#define CIRCULAR_LIST_H


#include <stdlib.h>


typedef struct _node
{
    struct _node* prev;
    struct _node* next;
    unsigned i, j, b;
    unsigned nbValeursPossibles;
} CIRCL_LIST;

CIRCL_LIST* createRoot (void);
CIRCL_LIST* circular_list_cons (CIRCL_LIST* root);
void circular_list_delete (CIRCL_LIST* node);
void deleteAll (CIRCL_LIST* root);

#endif

#ifndef ALEATOIRE_H
#define ALEATOIRE_H

#include <stdbool.h>

bool InitGenerateur (int min, int max);
int ReturnAleatoire (void);
void TermineGenerateur (void);

#endif


#ifndef _SUDOKU_H
#define _SUDOKU_H

#include <stdio.h>
#include <stdbool.h>


#define N       (3)
#define N_2     (N*N)
#define TOTAL_GRID_SZ   (N_2*N_2)
#define base    (1)

/**    Variables globales     **/
bool ligne[N_2][N_2];
bool colonne[N_2][N_2];
bool bloc[N_2][N_2];



/**     Fonctions     **/
/// sortie sur un flux
void OutputSudoku (int grille[N_2][N_2], FILE* fp);
/// sortie au format png. utilise cairo pour le rendu
void OutputSudokuToPng (int grid[N_2][N_2], const char* fontname, const char* output_filename);

/// genere une grille pleine remplie aleatoirement
bool GenerationBacktracking (int grille[N_2][N_2], int pos, unsigned aleatoire[][N_2]);
/// genere une grille jouable (1 seule solution) remplie aleatoirement.
// utilise un DLX
unsigned GenerationAleatoire (int grille[N_2][N_2]);
/// genere une grille jouable (1 seule solution) remplie aleatoirement.
// utilise un backtracking optimis� (avec une liste circulaire)
unsigned GenerationAleatoire2 (int grille[N_2][N_2]);


#endif
